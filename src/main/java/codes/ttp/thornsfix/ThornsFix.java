package codes.ttp.thornsfix;

import com.destroystokyo.paper.event.player.PlayerAttackEntityCooldownResetEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class ThornsFix extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onPlayerAttackEntityCooldownReset(final PlayerAttackEntityCooldownResetEvent e) {
        final var damageEvent = e.getAttackedEntity().getLastDamageCause();
        if (damageEvent != null && damageEvent.getCause() == EntityDamageEvent.DamageCause.THORNS) {
            e.setCancelled(true);
        }
    }
}
